# PuppetTerraformIntegration

This assessment aims to create infrastructure for a cloud provider using Terraform and trigger puppet configuration management files for automation.

## Tools Required:

- Terraform: Install Terraform on your system
- AWS: Any cloud provider
- Mobaxtream/ AWS CLI: For remote computing 

## Process:

- To trigger the puppet configuration management , we need to provide remote-exec
- The remote-exec provisioner invokes a script on a remote resource after it is created. 
- This can be used to run a configuration management tool.
- To invoke a local process, use the local-exec provisioner instead. 
- The remote-exec provisioner requires a connection and supports both ssh and winrm.

Syntax:

```
resource "aws_instance" "web" {
 
  connection {
    type     = "ssh"
    user     = "root"
    password = var.root_password
    host     = self.public_ip
  }

  provisioner "remote-exec" {
    inline = [
      "puppet apply",
      "consul join ${aws_instance.web.private_ip}",
    ]
  }
}
```


- Create a puppet manifest file and store into the correct directory
- To execute Terraform code:

```
Terraform init #initialize 
Terraform validate #validates the code
Terraform plan #shows the changes that are going to happen
Terraform apply #The terraform will be executed and the 
```



- By executing the terraform file, you can able to see the output in the terminal
- Terraform will create ec2_instance, install puppet, creeate manifest file and apply the manifest file

## Troubleshooting tips:

- Check Terraform Output: Review Terraform output and logs for any errors or warnings during provisioning.
- Verify SSH Connectivity: Ensure that SSH connectivity to provisioned instances is established correctly. Check SSH key permissions and network configurations.
- Inspect Puppet Logs: Examine Puppet logs (/var/log/messages, /var/log/puppetlabs/puppet/puppet.log) on provisioned instances for any errors or failures during configuration management.
- Monitor Resource Utilization: Monitor resource utilization (CPU, memory, disk) on provisioned instances to identify performance issues or resource constraints that may affect Puppet
- Check network inbound and outbound: Manually configure  the network traffic [ inbound and outbound] for any network issue, and allow access to port 80 [TCP] 
