provider "aws" {
  region = "us-east-1"
  access_key = "replace ur access key"
  secret_key = "replace ur secret key"

}


# Provision the EC2 instance
resource "aws_instance" "puppet" {
  ami                    = "ami-0440d3b780d96b29d"
  instance_type          = "t2.micro"
  key_name               = "windowsKey" 

  tags = {
    Name = "puppet"
  }
}

#Provision to install_puppet

resource "null_resource" "install_puppet"{

  depends_on = [aws_instance.puppet]

  provisioner "remote-exec" {
    inline = [
      "sudo yum update -y",
      "sudo yum install -y http://yum.puppetlabs.com/puppet8-release-amazon-2023.noarch.rpm",
      "sudo yum install -y puppet-agent",
      "sudo systemctl start puppet",
      "sudo systemctl enable puppet",
      "sudo /opt/puppetlabs/bin/puppet module install puppet-nginx --version 5.0.0"

    ]

    connection {
      type        = "ssh"
      user        = "ec2-user"                                    
      private_key = file("C:/Users/Shivapriya PS/Downloads/windowsKey.pem")
      host        = aws_instance.puppet.public_ip               
    }
  }
}

# Provison to apply puppet manifest

resource "null_resource" "apply_puppet_manifest"{

  depends_on = [null_resource.install_puppet]
  
  provisioner "file"{
	source = "S:/Worldline/Worldline_training/Projects/terraform-proj/puppetConfig.pp"
    	destination = "/home/ubuntu//etc/puppetlabs/code/environments/production/manifests/puppetconfig.pp"

  provisioner "remote-exec" {
    inline = [ 
      "sudo /opt/puppetlabs/bin/puppet apply puppetConfig.pp", 
      "sudo systemctl status nginx"
    ]
    connection {
      type        = "ssh"
      user        = "ec2-user"
      private_key = file("C:/Users/Shivapriya PS/Downloads/windowsKey.pem")
      host        = aws_instance.puppet.public_ip
    }
  }

}


