# Install nginx package

package { 'nginx':
  ensure => installed,
}


# Start and enable nginx service
service { 'nginx':
  ensure    => 'running',
  enable    => true,
}
